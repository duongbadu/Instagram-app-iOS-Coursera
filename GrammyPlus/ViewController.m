//
//  ViewController.m
//  GrammyPlus
//
//  Created by du Duong on 5/10/16.
//  Copyright © 2016 Dat Viet. All rights reserved.
//

#import "ViewController.h"
#import "NXOAuth2.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *logoutBtn;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.logoutBtn.enabled = false;
    self.refreshBtn.enabled = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginBtnPressed:(id)sender {
    [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Instagram"];
    
    self.loginBtn.enabled = false;
    self.logoutBtn.enabled = true;
    self.refreshBtn.enabled = true;
}

- (IBAction)logoutBtnPressed:(id)sender {
    NXOAuth2AccountStore* store = [NXOAuth2AccountStore sharedStore];
    NSArray* accounts = [store accountsWithAccountType:@"Instagram"];
    for (id acc in accounts) {
        [store removeAccount:acc];
    }
    
    self.loginBtn.enabled = true;
    self.logoutBtn.enabled = false;
    self.refreshBtn.enabled = false;
}

- (IBAction)refreshBtnPressed:(id)sender {
    NSArray* accounts = [[NXOAuth2AccountStore sharedStore] accountsWithAccountType:@"Instagram"];
    if (accounts.count <= 0) {
        return;
    }
    NXOAuth2Account *acc = accounts[0];
    NSString *token = acc.accessToken.accessToken;
    NSString *urlStr = [@"https://api.instagram.com/v1/users/self/media/recent/?access_token=" stringByAppendingString:token];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSLog(@"data request callback...");
        
        //check network error
        if (error) {
            NSLog(@"couldn't finish request: %@", error);
            return;
        }
        
        //check http error
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        if (httpResp.statusCode < 200 || httpResp.statusCode >= 300) {
            NSLog(@"http error: %ld", (long)httpResp.statusCode);
            return;
        }
        
        //check json parse error
        NSError *parseErr;
        id pkg = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseErr];
        if (!pkg) {
            NSLog(@"parse json error: %@", parseErr);
            return;
        }
        
        NSLog(@"pkg is: %@", pkg);
        NSString *imgUrlStr = pkg[@"data"][0][@"images"][@"standard_resolution"][@"url"];
        NSURL *imgUrl = [NSURL URLWithString:imgUrlStr];
        [[session dataTaskWithURL:imgUrl completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            
            //check network error
            if (error) {
                NSLog(@"couldn't finish request: %@", error);
                return;
            }
            
            //check http error
            NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
            if (httpResp.statusCode < 200 || httpResp.statusCode >= 300) {
                NSLog(@"http error: %ld", (long)httpResp.statusCode);
                return;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imageView.image = [UIImage imageWithData:data];
            });
            
        }] resume];
        
    }] resume];
}


@end
