//
//  AppDelegate.h
//  GrammyPlus
//
//  Created by du Duong on 5/10/16.
//  Copyright © 2016 Dat Viet. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

